# Augury webapp

Clone this repository to a place on your computer where you'd like the code to live:
```
git clone git@bitbucket.org:tcroxson/augury.git
```

The Augury webapp is designed to run on a Tomcat 7 server. You'll need to install Tomcat on your machine if you don't already have it. See [this link](http://tomcat.apache.org/) for instructions on installing Tomcat.

## Dependencies

The screenshot capture tool requires access to a headless web browser called PhantomJS. You can download PhantomJS [here](http://phantomjs.org/). 

Once PhantomJS is installed, set the `PHANTOMJS_PATH` context parameter in `webapp/WebContent/WEB-INF/web.xml` to the path where the PhantomJS executable is installed on your machine. This value defaults to `/usr/local/bin/phantomjs`.

## Running the Application

With Tomcat and PhantomJS installed, you can run the application from within the Eclipse Java IDE. (See [here](https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon2) for download instructions).

Start Eclipse and import the Augury project that you cloned into your workspace. Right click on the project's folder, and choose `Run As -> Run on Server`. Next, with `Manually define a new server` selected, Choose `Apache -> Tomcat v7.0 server` and click `Next`. Then select the directory where you installed Tomcat above under `Tomcat installation directory:`. Then click `Finish`.

The application should now be accessible in a broswer at:
```
localhost:8080/webapp/web/app.html
```

## Configuration

The app has two data views (a choropleth map and an accordion list) that you can switch between. The app will default to the map; to change this, open the file `WebContent/web/services/config.js` and change the order of the views listed under `activeDataViews`.

The app will use whichever view is listed first under `activeDataViews`. Although it would be possible to switch among views in-app, the UI for this has not been developed yet.