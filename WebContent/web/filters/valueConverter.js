angular.module("augury")
	.filter("valueConverter", function($filter) {
		return function(inputValue, inputType) {
			if (inputType == "percentage" && inputValue <= 1) {
				return $filter("number")(inputValue * 100, 1) + "%";
			} else if (inputType == "percentage" && inputValue > 1) {
				return $filter("number")(inputValue, 1) + "%";
			} else if (inputType == "value") {
				return $filter("number")(inputValue, 0);
			} else if (inputType == "valueRange") {
				return $filter("number")(inputValue, 2);
			}
		}	
	});