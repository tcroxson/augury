angular.module("augury")
	.filter("nameConverter", function() {
		return function(inputName) {
			var names = {
				"compositeScores": "Composite scores",
				"colors": "Image colors",
				"HSV": "Hue, saturation, value",
				"text": "Text areas",
				"quadtreeDecomposition": "Quadtree decomposition",
				"images": "Image areas",
				"other": "Other",
				"symmetry": "Symmetry",
				"balance": "Balance",
				"equilibrium": "Equilibrium",
		    	"colorfulnessScore": "Colorfulness score",
		    	"complexityScore": "Complexity score",
		    	"black": "Black",
		    	"silver": "Silver",
		    	"gray": "Gray",
		    	"white": "White",
		    	"maroon": "Maroon",
		    	"red": "Red",
		    	"purple": "Purple",
		    	"fuchsia": "Fuchsia",
		    	"green": "Green",
		    	"lime": "Lime",
		    	"olive": "Olive",
		    	"yellow": "Yellow",
		    	"navy": "Navy",
		    	"blue": "Blue",
		    	"teal": "Teal",
		    	"aqua": "Aqua",
		    	"hue": "Hue",
		    	"saturation": "Saturation",
		    	"value": "Value",
		    	"textArea": "Text area",
		    	"nonTextLeavesArea": "Non-text area",
		    	"numOfTextGroup": "Number of text groups",
		    	"maxLevel": "Maximum decomposition level",
		    	"avgLevel": "Average decomposition level",
		    	"numOfLeaves": "Number of decomposition leaves",
		    	"numOf1stLevelNodes": "Number of 1st-level decomposition nodes",
		    	"numOf2ndLevelNodes": "Number of 2nd-level decomposition nodes",
		    	"numOf3rdLevelNodes": "Number of 3rd-level decomposition nodes",
		    	"percentageLeafArea": "Leaf area percentage 1",
		    	"percentageLeafArea2": "Leaf area percentage 2",
		    	"numOfImageArea": "Number of image areas",
		    	"colorfulness1": "Colorfulness 1",
		    	"colorfulness2": "Colorfulness 2",
		    	"colorNumQuadTreeLeaves": "Number of color quadtree leaves",
		    	"intensityNumQuadTreeLeaves": "Number of intensity quadtree leaves",
		    	"colorHorizontalSymmetry": "Horizontal symmetry: color",
		    	"colorVerticalSymmetry": "Vertical symmetry: color",
		    	"intensityHorizontalSymmetry": "Horizontal symmetry: intensity",
		    	"intensityVerticalSymmetry": "Vertical symmetry: intensity",
		    	"colorHorizontalBalance": "Horizontal balance: color",
		    	"colorVerticalBalance": "Vertical balance: color",
		    	"intensityHorizontalBalance": "Horizontal balance: intensity",
		    	"intensityVerticalBalance": "Vertical balance: intensity",
		    	"colorEquilibrium": "Color equilibrium",
		    	"intensityEquilibrium": "Intensity equilibrium"
			}
			
			return names[inputName] || "";	
		}	
	});