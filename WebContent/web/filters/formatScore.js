angular.module("augury")
	.filter("formatScore", function() {
		return function(score) {
			return score * 10 + "%";
		}	
	});