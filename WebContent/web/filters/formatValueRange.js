angular.module("augury")
	.filter("formatValueRange", function() {
		return function(score, range) {
			return (score / (range[1] - range[0])) * 100 + "%";
		}	
	});