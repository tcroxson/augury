angular.module("augury")
	.directive("auguryMap", ["$sce", function($sce) {
		return {
	    	restrict: "E",
	    	link: function(scope, element, attrs) {	    		
	        	var hasLoaded = false, 
	        	s = 4,
				translation,
				width = 790,
				height = 445,
	        	proj = d3.geo.eckert5()
					.center([44,0])
					.scale(165),
				svg = d3.select(element[0])
					.style("opacity", "1")
					.append("svg")
					.attr("width", width)
					.attr("height", height)
					.classed("map-svg", true),
				color = d3.scale.quantize()
					.domain([0, 10])
					.range(scope.mapColorData),
				colorMap = function(d) {
					if (d.peak_appeal_color == "undef" || d.peak_appeal_complexity == "undef") {
						return "#eeeeee";
					} else if (d.peak_appeal_color == "na" || d.peak_appeal_complexity == "na") {
						return "#eeeeee";
					} else {
						return color(Math.abs(d.peak_appeal_color - scope.colorfulnessScore) + Math.abs(d.peak_appeal_complexity - scope.complexityScore));
					}   
				},
	        	path = d3.geo
					.path()
					.projection(proj),
				g = svg.append("g"),
				popup = d3.selectAll(".data-popup"),
				chart_title = svg.append("text")
					.attr("x", 10)
					.attr("y", 30)
					.attr("font-family", "Optima")
					.attr("font-size", "18px")
					.attr("fill", "#555555")
					.attr("font-weight", "bold"),
				zoom = d3.behavior.zoom()
			    	.scaleExtent([1, 10])
			    	.on("zoom", function() {
				    	move();
			    	}),
				msgQualifiers = function(diff, metricName) {			
	        		if (diff < -5 && diff > -30) {
						return "<em>decrease</em> your image's " + metricName + " a little";
	        		} else if (diff <= -30) {
						return "<em>decrease</em> your image's " + metricName + " a lot";
	        		} else if (diff > 5 && diff < 30) {
						return "<em>increase</em> your image's " + metricName + " a little";
	        		} else if (diff >= 30) {
						return "<em>increase</em> your image's " + metricName + " a lot";
	        		} else if (Math.abs(diff) <= 5) {
						return "keep your image's " + metricName + " the same";
	        		} else {
	        			return "";
	        		}
				},
				popupMsg = function(colorDiff, complexityDiff) {
					if (isNaN(colorDiff) && isNaN(complexityDiff)) {
						return "No preference data for this country.";
					}
					if (Math.abs(colorDiff) <= 5 && Math.abs(complexityDiff) <= 5) {
						return "The colorfulness and complexity of your image match this country's aesthetic preferences already!";
					}
					var msg = "Your image may become more appealing to people in this country if you ";
					msg += msgQualifiers(colorDiff, "colorfulness") + " and " + msgQualifiers(complexityDiff, "complexity") + ".";
					return msg;
				},
				// code for correctly zooming to center of map due to http://bl.ocks.org/linssen/7352810
				zoomButtons = function() {
					d3.selectAll(".zoom-button").on("click", function() {
						popup.transition();
						
						var clicked = d3.event.target,
				        direction = 1,
				        factor = .25,
				        target_zoom = 1,
				        center = [width / 2, height / 2],
				        extent = zoom.scaleExtent(),
				        translate = zoom.translate(),
				        translate0 = [],
				        l = [],
				        view = {x: translate[0], y: translate[1], k: zoom.scale()};
			
					    d3.event.preventDefault();
					    direction = (d3.select(this).classed("plus-button")) ? 1 : -1;
					    target_zoom = zoom.scale() * (1 + factor * direction);
			
					    if (target_zoom < extent[0] || target_zoom > extent[1]) { 
					    	return false; 
					    }
			
					    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
					    view.k = target_zoom;
					    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];
	
					    view.x += center[0] - l[0];
					    view.y += center[1] - l[1];
	
					    interpolateZoom([view.x, view.y], view.k);
					});
				},
				interpolateZoom = function(translate, scale) {
				    var self = this;
				    return d3.transition().duration(350).tween("zoom", function () {
				        var iTranslate = d3.interpolate(zoom.translate(), translate),
				        iScale = d3.interpolate(zoom.scale(), scale);
				        return function (t) {
				            zoom.scale(iScale(t))
				            	.translate(iTranslate(t));
				            move();
				        };
				    });
				},
				move = function() {
					// hide the popup on any map move
					popup.style("display", "none");
					
				    var t = zoom.translate(),
				    s = zoom.scale(),
				    scale = d3.scale.linear()
				   		.domain(zoom.scaleExtent())
				   		.range([$(".zoom-bar line").attr("y2") - $(".zoom-bar line").attr("y1"), 0]);
				    
				    t[0] = Math.min(width / 2 * (s - 1) + 255 * s, Math.max(width / 2 * (1 - s) - 255 * s, t[0]));
				    t[1] = Math.min(height / 2 * (s - 1) + 230 * s, Math.max(height / 2 * (1 - s) - 230 * s, t[1]));
				    zoom.translate(t);
				    translation = t;
				    g.attr("transform", "translate(" + t + ")scale(" + s + ")");
				    g.selectAll("path")
				    	.style("stroke-width", 1 / (3 * s))
				    	.attr("d", path.projection(proj));
	
				    // move the zoombar indicator
				    $(".zoom-indicator").attr("y", scale(s));
				};

				d3.csv(scope.mapDataSource, function(data) {
					d3.json(scope.mapFileSource, function(error, topology) {
						for (var i = 14; i < data.length; i++) {
							country_code = data[i].iso_code;
							for (var j = 0; j < topology.objects.countries.geometries.length; j++) {
								if (topology.objects.countries.geometries[j].id == country_code) {
									topology.objects.countries.geometries[j].country_name = data[i].population;
									topology.objects.countries.geometries[j].peak_appeal_color = data[i].peak_appeal_color;
									topology.objects.countries.geometries[j].peak_appeal_complexity = data[i].peak_appeal_complexity;
									topology.objects.countries.geometries[j].description = data[i].description;
									break;
								}
							}
						}
		
						g.selectAll("path")
							.data(topojson.object(topology, topology.objects.countries).geometries)
							.enter()
							.append("path")
							.attr("d", path)
							.attr("centroid", function(d) {
						      return path.centroid(d);
						    })
						    .style("stroke", "#AAAAAA")
							.style("stroke-width", 1 / s)
							.on("click", function(d) {								
								popup.transition()
								     .duration(200);
								
								var popupWidth = $(".data-popup").width();
								var popupHeight = $(".data-popup").height();
								var mapYOffset = $(".data-container-body").offset()["top"];
								var mapXOffset = $(".data-container-body").offset()["left"];
	
								if (d3.event.pageX + popupWidth > window.innerWidth && d3.event.pageY + popupHeight > window.innerHeight) {
									popup.attr("class", "data-popup caret-lower-right")
										 .style("left", (d3.event.pageX - popupWidth - mapXOffset - 30) + "px")     
										 .style("top", (d3.event.pageY - popupHeight - (mapYOffset / 2) + 35) + "px")
										 .style("display", "block");
								} else if (d3.event.pageX + popupWidth > window.innerWidth) { 
									popup.attr("class", "data-popup caret-upper-right")
									 	 .style("left", (d3.event.pageX - mapXOffset - popupWidth - 30) + "px")     
										 .style("top", (d3.event.pageY - (mapYOffset / 2) - 30) + "px")
									 	 .style("display", "block");
								} else if (d3.event.pageY + popupHeight > window.innerHeight) {
									popup.attr("class", "data-popup caret-lower-left")
									 	 .style("left", (d3.event.pageX - mapXOffset + 40) + "px")     
									 	 .style("top", (d3.event.pageY - popupHeight - (mapYOffset / 2) + 40) + "px")
									 	 .style("display", "block");
								} else {
									popup.attr("class", "data-popup caret-upper-left")
										 .style("left", (d3.event.pageX - mapXOffset + 40) + "px")     
							             .style("top", (d3.event.pageY - (mapYOffset / 2) - 30) + "px")
							             .style("display", "block");		
								}
												
								var colorDiff = (d.peak_appeal_color - scope.colorfulnessScore) * 10;
								var complexityDiff = (d.peak_appeal_complexity - scope.complexityScore) * 10;

								// use $apply because we're updating scope bindings outside of angular's
								// regular digest cycle
								scope.$apply(function() {
									scope.complexityPref = d.peak_appeal_complexity;
									scope.colorfulnessPref = d.peak_appeal_color;
									scope.countryName = d.country_name;
									scope.popupMsg = $sce.trustAsHtml(popupMsg(colorDiff, complexityDiff));
								});
							})
							.style("fill", function(d) { return colorMap(d) });
						
							hasLoaded = true;
					});
				});
				
				svg.call(zoom);
				zoomButtons();
				
				// watch for changes in colorfulnessScore and complexityScore, which may be
				// triggered from the sidebar when a new image is uploaded, and re-color the
				// map when they happen
				scope.$watchGroup(["colorfulnessScore", "complexityScore"], function() {
					// this $watch expression will fire during initial map loading;
					// don't do anything in this case, as attempting to interact with the
					// map before it's completely loaded will result in an error
					if (hasLoaded) {
						g.selectAll("path")
							.style("fill", function(d) { return colorMap(d) });
					}
				});
				
				// hide popup if user clicks off the map
				// don't hide if they click on a popup that's already open
				$(document).on("click", function(e) {
					if (e.target.nodeName == "path" || ($(e.target).parents(".data-popup").length > 0))
						return;
					$(".data-popup").css("display", "none");
				});	
	    	}
        };
	}]);