angular.module("augury")
	.controller("auguryCtrl", function($scope, $http, $location) {
		$scope.getClass = function(path) {
			if ($location.path().substr(0, path.length) === path) {
				return "active";
			} else {
				return "";
			}
		}
		
		$scope.dataSplash = false;
		
		$scope.countryName = "";
		
		// configuration values
		$scope.config = {
			mapFileSource: "data/world-110m2-no-ant.json",
			mapDataSource: "data/summary_data.csv",
			mapColorData: ["#08306b", "#08519c", "#2171b5", "#4292c6", "#6baed6", "#9ecae1", "#c6dbef", "#deebf7", "#f7fbff"],
			mapHeight: "auto",   // <-- TO-DO: auto dimensions will be derived from map parent element
			mapWidth: "auto"
		}
		
		$scope.tooltips = {
			complexityBar: "Your complexity score reflects how many images and text areas your screenshot has.",
			colorfulnessBar: "Your colorfulness score reflects how many different colors are present in your screenshot.",
			complexityPreference: "This bar represents the website complexity preference of people from a given country.",
			colorfulnessPreference: "This bar represents the website colorfulness preference of people from a given country."
		}
		
		// modal feedback box functions
		$scope.open = function() {
			$scope.showFeedbackModal = true;
		};

		$scope.ok = function() {
			// attempt to submit data to database
			var data = {
				tableName: "comments",
				improvementComments: $("#improvement_comments").val(),
				otherComments: $("#other_comments").val()
			}
						
			$.ajax({
	 			type: "POST",
	 			url: "../databaseAccess",
	 			data: data
	 		});
			
			$scope.showFeedbackModal = false;
		};

		$scope.cancel = function() {
			$scope.showFeedbackModal = false;
		};
		
		$scope.closeDataSplash = function() {	
			$scope.showDataSplashModal = false;
		};
		
		$scope.openDataSplash = function() {
			$scope.showDataSplashModal = true;
		}
		
		$scope.checkDataSplash = function() {
			// check if we need to show data page splash screen modal
			if (!$scope.dataSplash) {
				$scope.dataSplash = true;
				$scope.showDataSplashModal = true;
			}	
		}
		
	});