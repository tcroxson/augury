angular.module("augury")
	.controller("metricsListCtrl", function($scope, utils) {

		$scope.metrics = utils.getImgInfo("metrics");
		$scope.isOpen = true;
		
		$scope.$on("imgInfoUpdated", function() {			
			$scope.metrics = utils.getImgInfo("metrics");
		});
	});