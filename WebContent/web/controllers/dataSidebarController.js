angular.module("augury")
	.controller("dataSidebarCtrl", function($scope, $http, utils, FileUploader) {		
		var setTemplateData;
		(setTemplateData = function() {
			$scope.complexityScore = utils.getImgInfo("complexityScore").value;
			$scope.colorfulnessScore = utils.getImgInfo("colorfulnessScore").value;
			$scope.uploadedImg = utils.getImgInfo("filename");
			$scope.isCropped = utils.getImgInfo("isCropped");
			return setTemplateData;
		})();

		$scope.remoteUrl = "";

		$scope.uploader = new FileUploader({
			url: "../imageServlet?method=uploadImage",
			method: "POST",
			autoUpload: true
		});

		$scope.uploader.onCompleteItem = function(item, response, status, headers) {			
			utils.setImgInfo(response);
			utils.removeLoadingIconDelayed();
			setTemplateData();
		};
		$scope.uploader.onAfterAddingFile = function(item) {
		}
		$scope.uploader.onErrorItem = function(item, response, status, headers) {
			//console.log(response);
		}
		$scope.uploader.onProgressItem = function(fileItem, progress) {
			$(".error-msg").css("display", "none");
			$scope.remoteUrl = "";
			utils.showLoadingIcon(".data-container");
        };

		$scope.submitRemoteUrl = function() {
			utils.showLoadingIcon(".data-container");
			$http.get("../screenShotServlet?method=getCaptureImage&url=" + $scope.remoteUrl)
				.success(function(data, status, headers, config) {					
					utils.setImgInfo(data);
					utils.removeLoadingIconDelayed();
					setTemplateData();
				})
				.error(function(data, status, headers, config) {
					console.log(data);
					
					$(".error-msg").css("display", "block");
					utils.removeLoadingIconDelayed();
				});
		}
	});