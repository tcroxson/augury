angular.module("augury")
	.controller("mapCtrl", function($scope, utils, $templateRequest, config, $uibModal, $document) {

		$scope.mapColorData = config.get("mapColorData");
		$scope.mapDataSource = config.get("mapDataSource");
		$scope.mapFileSource = config.get("mapFileSource");
		$scope.complexityScore = utils.getImgInfo("complexityScore").value;
		$scope.colorfulnessScore = utils.getImgInfo("colorfulnessScore").value;

		$scope.$on("imgInfoUpdated", function() {			
			$scope.complexityScore = utils.getImgInfo("complexityScore").value;
			$scope.colorfulnessScore = utils.getImgInfo("colorfulnessScore").value;
		});

		var dataSplashModal = $uibModal.open({
			animation: true,
			ariaLabelledBy: "modal-title",
			ariaDescribedBy: "modal-body",
			templateUrl: "views/mapSplash.html",
			size: "lg",
			controller: function($scope) {
				$scope.closeDataSplash = function() {					
					dataSplashModal.close();
				};  
		    },
			appendTo: angular.element($document[0].querySelector("body"))
	    });	
	});