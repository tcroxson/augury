angular.module("augury")
	.controller("landingPageCtrl", function($scope, $http, $rootScope, $location, utils, FileUploader) {
		$scope.remoteUrl = "";

		$scope.homeUploader = new FileUploader({
			url: "../imageServlet?method=uploadImage",
			method: "POST",
			autoUpload: true
		});
		$scope.homeUploader.onCompleteItem = function(item, response, status, headers) {			
			utils.setImgInfo(response);
			utils.removeLoadingIconDelayed();
			$location.path("/data");
		};
		$scope.homeUploader.onAfterAddingFile = function(item) {
		}
		$scope.homeUploader.onErrorItem = function(item, response, status, headers) {
			// console.log(response);
		}
		$scope.homeUploader.onProgressItem = function(fileItem, progress) {
			utils.showLoadingIcon(".landing-page");
        };
        $scope.submitRemoteUrl = function() {        	
        	$("#homeRemoteUrlError").css("display", "none");
			utils.showLoadingIcon(".landing-page");

			$http.get("../screenShotServlet?method=getCaptureImage&url=" + $scope.remoteUrl)
				.success(function(data, status, headers, config) {					
					utils.removeLoadingIconDelayed();
					utils.setImgInfo(data);
					$location.path("/data");
				})
				.error(function(data, status, headers, config) {
					console.log(data);
					
					$(".error-msg").css("display", "block");
					utils.removeLoadingIconDelayed();
				});
		}
	})