/*************************************************************
 * generateScreencap.js
 * 
 * Via phantomjs, renders the passed url to a base64 encoded 
 * stream and sends this to standard output for use by the
 * Java servlet. Also saves the image as a jpg.
 *
 * Author: Trevor Croxson
 *
 * Last Modified: December 17, 2016
 * 
 * © Copyright 2016 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

var page = require('webpage').create(),
	system = require('system');
var url = system.args[1];
var uploadPath = system.args[2];
var filename = system.args[3];
var protocol = url.match(/^(http:\/\/|https:\/\/)/i);
protocol = (protocol) ? "" : "http://";

page.viewportSize = {
	width: 1024,
	height: 768
};

page.clipRect = {
	top: 0, 
	left: 0, 
	width: 1024, 
	height: 768 
};


page.open(protocol + url, function() {});

page.onLoadFinished = function(status) {
	if (status == "success") {
		//page.render(uploadPath + filename + ".jpg", { format: "jpeg" });
	  
		// if the page loads successfully, write out the encoded image data
		//console.log(page.renderBase64('JPEG'));
		//phantom.exit();
		//system.stdout.write(page.renderBase64('JPEG'));
		
		// set a timeout to allow page content to load
		setTimeout(function(){
			page.render(uploadPath + filename + ".jpg", { format: "jpeg" });
		  
			console.log(page.renderBase64('JPEG'));
			//system.stdout.write(page.renderBase64('JPEG'));
			
			phantom.exit();
		}, 1500);
	} else {
		// otherwise, write out the string "fail"
		console.log(status);
	}
};

page.onResourceError = function(response) {
	console.log("fail: " + response.errorString);
};