angular.module("config", [])
	.service("config", function() {
		var config = {
			activeDataViews: ["views/map.html", "views/metricsList.html"],
			mapFileSource: "data/world-110m2-no-ant.json",
			mapDataSource: "data/summary_data.csv",
			mapColorData: ["#08306b", "#08519c", "#2171b5", "#4292c6", "#6baed6", "#9ecae1", "#c6dbef", "#deebf7", "#f7fbff"]
		}

		return {
			get: function(attr) {
				return config[attr] || {};
			}
		}
	});