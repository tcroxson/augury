angular.module("utils", [])
	.service("utils", function($rootScope) {

		var dummyMetrics = [
         	{
        		"categoryName": "compositeScores",
        		"items": [
        		    {
        		    	"name": "colorfulnessScore",
        		    	"value": 2.794095936,
        		    	"type": "valueRange",
        		    	"range": [0, 10]
        		    },
        		    {
        		    	"name": "complexityScore",
        		    	"value": 4.422553855,
        		    	"type": "valueRange",
        		    	"range": [0, 10]
        		    }
        		]
        	},
        	{
        		"categoryName": "HSV",
        		"items": [
        		    {
        		    	"name": "hue",
        		    	"value": 55.66401927,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "saturation",
        		    	"value": 37.96971512,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "value",
        		    	"value": 165.3721593,
        		    	"type": "value"
        		    }
        		]
        	},
        	{
        		"categoryName": "text",
        		"items": [
        		    {
        		    	"name": "textArea",
        		    	"value": 48905,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "nonTextLeavesArea",
        		    	"value": 412600,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "numOfTextGroup",
        		    	"value": 6,
        		    	"type": "value"
        		    }
        		]
        	},
        	{
        		"categoryName": "quadtreeDecomposition",
        		"items": [
        		    {
        		    	"name": "maxLevel",
        		    	"value": 5,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "avgLevel",
        		    	"value": 3.9375,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "numOfLeaves",
        		    	"value": 16,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "numOf1stLevelNodes",
        		    	"value": 1,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "numOf2ndLevelNodes",
        		    	"value": 5,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "numOf3rdLevelNodes",
        		    	"value": 10,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "percentageLeafArea",
        		    	"value": 58.68339539,
        		    	"type": "percentage"
        		    },
        		    {
        		    	"name": "percentageLeafArea2",
        		    	"value": 60.04410559,
        		    	"type": "percentage"
        		    }
        		]
        	},
        	{
        		"categoryName": "images",
        		"items": [
        		    {
        		    	"name": "numOfImageArea",
        		    	"value": 2,
        		    	"type": "value"
        		    }
        		]
        	},
        	{
        		"categoryName": "other",
        		"items": [
        		    {
        		    	"name": "colorfulness1",
        		    	"value": 44.32945078,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "colorfulness2",
        		    	"value": 4.552575166,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "colorNumQuadTreeLeaves",
        		    	"value": 1723,
        		    	"type": "value"
        		    },
        		    {
        		    	"name": "intensityNumQuadTreeLeaves",
        		    	"value": 56,
        		    	"type": "value"
        		    }
        		]
        	},
        	{
        		"categoryName": "symmetry",
        		"items": [
        		    {
        		    	"name": "colorHorizontalSymmetry",
        		    	"value": 0.841608683,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "colorVerticalSymmetry",
        		    	"value": 0.661595662,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "intensityHorizontalSymmetry",
        		    	"value": 0.96140035,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "intensityVerticalSymmetry",
        		    	"value": 0.96140035,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    }
        		]
        	},
        	{
        		"categoryName": "balance",
        		"items": [
        		    {
        		    	"name": "colorHorizontalBalance",
        		    	"value": 0.118938876,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "colorVerticalBalance",
        		    	"value": 0.44144969,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "intensityHorizontalBalance",
        		    	"value": 0.279426817,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "intensityVerticalBalance",
        		    	"value": 1,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    }
        		]
        	},
        	{
        		"categoryName": "equilibrium",
        		"items": [
        		    {
        		    	"name": "colorEquilibrium",
        		    	"value": 0.88671875,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    },
        		    {
        		    	"name": "intensityEquilibrium",
        		    	"value": 0.509765625,
        		    	"type": "valueRange",
        		    	"range": [0, 1]
        		    }
        		]
        	},
        	{
        		"categoryName": "messages",
        		"isCropped": false
        	}
        ];
		
		return {
			fadedElts: {},
			showLoadingIcon: function(elt, faded, size) {
				// NOTE: the CSS opacity rule is inherited by all child elements. 
				// It's not possible to override this behavior, so we append the icon 
				// to the body to avoid fading it out along with the target element.
				var x_pos = $(elt).offset().left,
					y_pos = $(elt).offset().top,
					height = $(elt).height(),
					width = $(elt).width(),
					faded = faded || true,
					size = size || 150;

				if (faded) {
					$(elt).css("opacity", ".3");
					this.fadedElts[elt] = true;
				}
				$("body")
					.prepend("<img src='img/augury_icon3.svg' class='loadingIcon' />");
				$(".loadingIcon")
					.attr("width", size)
					.attr("height", size)
					.css("position", "absolute")
					.css("left", x_pos + (width / 2) - (size / 2))
					.css("top", y_pos + (height / 2) - (size / 2))
					.css("z-index", "100")
					.css("display", "none")
					.fadeIn("slow");	
			},
			removeLoadingIcon: function() {
				var self = this;
				$(".loadingIcon").fadeOut("slow", function() {
					$(this).remove();
					angular.forEach(self.fadedElts, function(value, key) {
						$(key).css("opacity", "1");
					});
				});
			},
			removeLoadingIconDelayed: function() {
				var self = this;
				setTimeout(function() {
					self.removeLoadingIcon();
				}, 1500);
			},
			imgInfo: {
				filename: "img/placeholderImg.png",
				metrics: dummyMetrics
			},
			setImgInfo: function(data) {
				var self = this;
				angular.forEach(data, function(value, key) {
					self.imgInfo[key] = value;
				});
				$rootScope.$broadcast("imgInfoUpdated");
			},
			// Search the JSON object of image information in the following way:
			// - first, if no search term is passed, return the entire JSON object 
			// - second, search top-level keys (such as filename)
			// - third, if no top-level key is found, search the metrics structure,
			//   returning a category group or an individual metric
			getImgInfo: function(target) {				
				if (!target) return this.imgInfo;
				var self = this,
					foundTarget = null;
								
				Object.keys(this.imgInfo).forEach(function(topLevelItem) {					
					if (topLevelItem === target) {
						foundTarget = self.imgInfo[target]; 
					}		
				});
				
				this.imgInfo.metrics.forEach(function(metricCategory) {
					if (metricCategory.categoryName === target) {
						foundTarget = metricCategory;
					}
										
					if (metricCategory.items) {
						metricCategory.items.forEach(function(item) {
							if (item.name === target) {
								foundTarget = item;
							}
						});
					}
				});

				return foundTarget;
			}
		}
	});