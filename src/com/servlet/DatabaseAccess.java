package com.servlet;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
 
public class DatabaseAccess extends HttpServlet {
  
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
		Enumeration parameters = request.getParameterNames();
		String tableName = "";
		ArrayList<String> fields = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		int i;
	  
		// loop through supplied parameters
		while (parameters.hasMoreElements()) {
			String parameter = (String)parameters.nextElement();
			String value = request.getParameter(parameter);
		  
			if (parameter.equals("tableName")) {
				tableName = value;
			} else {
				fields.add(parameter);
				values.add(value);
			}		  
		}
	  
		/*
		System.out.println(tableName);
		for (String field : fields) {
			System.out.println(field);
		}
		for (String value : values) {
			System.out.println(value);
		}
		*/
	  
		// JDBC driver name and database URL
		final String JDBC_DRIVER="com.mysql.jdbc.Driver";  
		final String DB_URL="jdbc:mysql://localhost:8889/augury";

		// Database credentials
		final String USER = "root";
		final String PASS = "root";

		// Set response content type
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// Open a connection
			java.sql.Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// Build and execute SQL query
			java.sql.PreparedStatement stmt = null;
			String sql;
			sql = "INSERT INTO " + tableName + " (";
			for (String field : fields) {
				sql += "`" + field + "`, ";
			}
			i = sql.length();
			sql = sql.substring(0, i-2);
			
			sql += ") VALUES (";			
			for (String value : values) {
				sql += "?, ";
			}
			i = sql.length();
			sql = sql.substring(0, i-2);
			sql += ")";
			
			System.out.println(sql);
			
			stmt = conn.prepareStatement(sql);
			int position = 1;
			for (String value : values) {
				stmt.setString(position, value);
				position++;
			}
			
			stmt.executeUpdate();
			
			// Clean-up environment
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			//Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			//Handle errors for Class.forName
			e.printStackTrace();
		}
	}
} 