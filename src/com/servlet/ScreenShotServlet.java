package com.servlet;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import sun.misc.BASE64Decoder;

import com.util.ImageCapture;

public class ScreenShotServlet extends ImageProcessServlet {
	@SuppressWarnings("unchecked")
    public void getCaptureImage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Process phantomjs;
        try {
            request.setCharacterEncoding("UTF-8");
            String website = request.getParameter("url");
            String basePath = this.getServletContext().getRealPath("/");            
            String imageUploadDirectory = getImageUploadDirectory();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
            String imageFilename = formatter.format(new Date());
			imageFilename += System.currentTimeMillis();
			
            phantomjs = Runtime.getRuntime().exec(PHANTOMJS_PATH + " " + basePath + "web/controllers/generateScreencap.js " + website + " " + imageUploadDirectory + " " + imageFilename);

    		BufferedReader stdInput = new BufferedReader(new InputStreamReader(phantomjs.getInputStream()));
    		String imgData = stdInput.readLine();
    		
    		if (imgData.startsWith("fail")) {
    			stdInput.close();
    			phantomjs.destroy();
    			JSONObject dataFail = new JSONObject(); 
    			dataFail.put("success", false);
    			dataFail.put("message", imgData);
    			response.setStatus(500);
    			writeOut(response, dataFail);
    		} else {            	
                response.setContentType("text/html; charset=UTF-8");
                response.setCharacterEncoding("UTF-8");                 
                BufferedImage buffImg = null;
                BASE64Decoder decoder = new BASE64Decoder();
                byte[] imageBytes = decoder.decodeBuffer(imgData);                 
                ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);  
                buffImg = ImageIO.read(bis);              
                // displays the decoded image in a canvas window
                /*
                JFrame frame = new JFrame();
                frame.getContentPane().setLayout(new FlowLayout());
                frame.getContentPane().add(new JLabel(new ImageIcon(buffImg)));
                frame.setVisible(true);
                */
				JSONObject returnJSON = new JSONObject();
                returnJSON = processImage(imageUploadDirectory + imageFilename + ".jpg");
                stdInput.close();
                returnJSON.put("filename", "../" + UPLOADS_FOLDER + imageFilename + ".jpg");
                returnJSON.put("success", true);

                phantomjs.destroy();
                
                writeOut(response, returnJSON);
    		}
        } catch (Exception e) {
    		System.out.println(e);
    		e.printStackTrace();
        }
    }
}