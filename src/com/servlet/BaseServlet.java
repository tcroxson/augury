package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;


public class BaseServlet extends HttpServlet{
		
	public static String UPLOADS_FOLDER;
	public static String PHANTOMJS_PATH;
	
	public void init() throws ServletException {
	    UPLOADS_FOLDER = getServletContext().getInitParameter("UPLOADS_FOLDER");
	    PHANTOMJS_PATH = getServletContext().getInitParameter("PHANTOMJS_PATH");
	}
	
	@Override
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		this.doPost(arg0, arg1);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String requestedMethod = request.getParameter("method");
		try {
			Method imageProcessMethod = this.getClass().getMethod(requestedMethod, new Class[]{HttpServletRequest.class, HttpServletResponse.class});
			imageProcessMethod.invoke(this, new Object[]{request, response});
		} catch (Exception e) {
			throw new ServletException("cannot find the method " + requestedMethod + "!", e);
		}
	}
	
	protected void writeOut(HttpServletResponse response, JSONObject data) {
		PrintWriter out = null;
		try {			
			response.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			out.write(data.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
	
	protected String getImageUploadDirectory() {
		String imageUploadDirectory = this.getServletContext().getRealPath("/") + UPLOADS_FOLDER;
		File file = new File(imageUploadDirectory);
		if (!file.isDirectory()) {
			file.mkdir();	
		}
		
		return imageUploadDirectory;
	}
}
