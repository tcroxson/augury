package com.servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.upload.SingleFileUpload;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ImageServlet extends ImageProcessServlet {

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unchecked")
	public void uploadImage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String imageUploadDirectory = getImageUploadDirectory();
		String imagePath, imageFileExt;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		String imageFilename = formatter.format(new Date());

		int imageMaxSize = Integer.parseInt(this.getInitParameter("FILE_SIZE_LIMIT")) * 1024 * 1024;

		SingleFileUpload sf = SingleFileUpload.getInstance();
		sf.setEncoding("UTF-8");
		sf.parseRequest(request);

		if (sf.getFileItem() != null && sf.getFileItem().getSize() > 0) {
			sf.setSizeMax(imageMaxSize); 
			sf.setRepository(new File(System.getProperty("java.io.tmpdir")));
			long lngLength = sf.getFileItem().getSize();
			if (lngLength > imageMaxSize) {
				//writeOut(response, "{\"fileID\":null,\"msg\":\"file size cannot be larger than " + fileMaxSize + "M\"}");
				throw new RuntimeException("file size cannot be larger than " + imageMaxSize + "M");
			} else {
				imageFileExt = sf.getFileItem().getName();
				imageFileExt = imageFileExt.substring(imageFileExt.lastIndexOf(".") + 1);
				imageFilename += System.currentTimeMillis() + "." + imageFileExt;
				imagePath = imageUploadDirectory + imageFilename;

				JSONObject returnJSON = new JSONObject();

				try {
					// start uploading image
					File file = sf.upload(imagePath);										
					returnJSON = processImage(imagePath);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				request.getSession().setAttribute("filename", imageFilename);
				returnJSON.put("filename", "../" + UPLOADS_FOLDER + imageFilename);

				writeOut(response, returnJSON);
			}
		} else {
			throw new RuntimeException("No image uploaded");
		}
	}
}
