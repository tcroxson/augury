package com.servlet;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import javax.imageio.ImageIO;

import org.sikuli.design.QuadtreeFeatureComputer;
import org.sikuli.design.XYFeatureComputer;
import org.sikuli.design.color.ColorAnalyzer;
import org.sikuli.design.color.NamedColor;
import org.sikuli.design.color.StandardColors;
import org.sikuli.design.quadtree.ColorEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.IntensityEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.QuadTreeDecomposer;
import org.sikuli.design.quadtree.QuadTreeDecompositionStrategy;
import org.sikuli.design.quadtree.Quadtree;
import org.sikuli.design.structure.Block;
import org.sikuli.design.xycut.DefaultXYDecompositionStrategy;
import org.sikuli.design.xycut.XYDecomposer;
import org.sikuli.design.xycut.XYDecompositionStrategy;
import org.sikuli.design.xycut.XYTextDetector;

import com.googlecode.javacv.cpp.opencv_core.CvScalar;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

public class ImageProcessServlet extends BaseServlet {

	public JSONObject processImage(String uploadedImagePath) throws IOException {
		try {
			BufferedImage uploadedImage = ImageIO.read(new File(uploadedImagePath));
			boolean isCropped = false;
			
			// crop oversize images
			if (uploadedImage.getHeight() > 768 || uploadedImage.getWidth() > 1024) {
				isCropped = true;
				BufferedImage croppedImage = uploadedImage.getSubimage(0, 0, 1024, 768);
				JSONObject returnJSON = getImageMetrics(croppedImage, uploadedImagePath, isCropped);
				return returnJSON;
			} else {
				JSONObject returnJSON = getImageMetrics(uploadedImage, uploadedImagePath, isCropped);
				return returnJSON;
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
			return new JSONObject();
		}
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject buildMetricJSON(String metricName, double metricValue, String metricType, Integer... range) {
		JSONObject item = new JSONObject();
		item.put("name", metricName);
		item.put("value", metricValue);
		item.put("type", metricType);
		
		if (metricType.equals("valueRange")) {
			JSONArray rangeArray = new JSONArray();
			rangeArray.add(range[0]);
			rangeArray.add(range[1]);
			item.put("range", rangeArray);
		}
		
		return item;
	}

	@SuppressWarnings("unchecked")
	private JSONObject getImageMetrics(BufferedImage input, String inputFilename, boolean isCropped) throws IOException {
		String baseFilename = inputFilename.substring(inputFilename.lastIndexOf(".") + 1);
		String outputFilename = baseFilename + ".xml";	
		JSONArray metrics = new JSONArray();

		decomposeSingleImage(input, inputFilename, outputFilename);
		StandardColors colorNames = new StandardColors();
		File file = new File(inputFilename);

		try {
			FileWriter csvWriter = new FileWriter(baseFilename + ".csv");
			csvWriter.append("filename,");
			
			for (NamedColor c : colorNames) {
				csvWriter.append(c.getName() + ",");
			}

			csvWriter.append("hue,saturation,value,textArea,nonTextArea,maxLevel,"
				+ "averageLevel,numOfLeaves,numOf1stLevelNodes,"
				+ "numOf2ndLevelNodes,numOf3rdLevelNodes,percentageOfLeafArea,"
				+ "percentageOfLeafArea2,numOfTextGroup,numOfImageArea,"
				+ "colorfulness1,colorfulness2,colorNumQuadTreeLeaves,"
				+ "intensityNumQuadTreeLeaves,colorHorizontalSymmetry,colorVerticalSymmetry,"
				+ "colorHorizontalBalance,colorVerticalBalance,"
				+ "intensityHorizontalSymmetry,intensityVerticalSymmetry,"
				+ "intensityHorizontalBalance,intensityVerticalBalance,colorEquilibrium,"
				+ "intensityEquilibrium,colorfulnessScore,complexityScore\n"
			);

			csvWriter.append(file.getName() + ",");
			Map<NamedColor, Double> d = ColorAnalyzer.computeColorDistribution(input);

			double colorGray = d.get(colorNames.colors.get(2));
			double colorWhite = d.get(colorNames.colors.get(3));
			double colorMaroon = d.get(colorNames.colors.get(4));
			double colorGreen = d.get(colorNames.colors.get(8));
			double colorLime = d.get(colorNames.colors.get(9));
			double colorBlue = d.get(colorNames.colors.get(13));
			double colorTeal = d.get(colorNames.colors.get(14));

			for (NamedColor c : colorNames.colors) {
				csvWriter.append(d.get(c) + ",");
			}

			CvScalar avg = ColorAnalyzer.computeAverageHueSaturationValue(input);
			double colorHue = avg.getVal(0);
			double colorSaturation = avg.getVal(1);
			double colorValue = avg.getVal(2);
			
			csvWriter.append(colorHue + ","
				+ colorSaturation + ","
				+ colorValue + ","
			);

			File xmlfile = new File(outputFilename);
			Block rootWithTextdetected = Block.loadFromXml(xmlfile.getAbsolutePath());
			QuadTreeDecompositionStrategy iStrategy = new IntensityEntropyDecompositionStrategy();
			QuadTreeDecompositionStrategy cStrategy = new ColorEntropyDecompositionStrategy();
			QuadTreeDecomposer cDecomposer = new QuadTreeDecomposer(cStrategy);
			Quadtree cRoot = cDecomposer.decompose(input);
			QuadTreeDecomposer iDecomposer = new QuadTreeDecomposer(iStrategy);
			Quadtree iRoot = iDecomposer.decompose(input);
			Point centerOfImage = new Point(input.getWidth() / 2, input.getHeight() / 2);
			
			// image metrics
			int textArea = XYFeatureComputer.computeTextArea(rootWithTextdetected);
			int nonTextLeavesArea = XYFeatureComputer.computeNonTextLeavesArea(rootWithTextdetected);
			int numOfTextGroup = XYFeatureComputer.countNumberOfTextGroup(rootWithTextdetected);
			int maxLevel = XYFeatureComputer.computeMaximumDecompositionLevel(rootWithTextdetected);
			double avgLevel = XYFeatureComputer.computeAverageDecompositionLevel(rootWithTextdetected);
			int numOfLeaves = XYFeatureComputer.countNumberOfLeaves(rootWithTextdetected);
			int numOf1stLevelNodes = XYFeatureComputer.countNumberOfNodesInLevel(rootWithTextdetected, 1);
			int numOf2ndLevelNodes = XYFeatureComputer.countNumberOfNodesInLevel(rootWithTextdetected, 2);
			int numOf3rdLevelNodes = XYFeatureComputer.countNumberOfNodesInLevel(rootWithTextdetected, 3);
			double percentageLeafArea = XYFeatureComputer.computePercentageOfLeafArea(rootWithTextdetected);
			double percentageLeafArea2 = XYFeatureComputer.computePercentageOfLeafArea2(rootWithTextdetected);
			int numOfImageArea = XYFeatureComputer.countNumberOfImageArea(rootWithTextdetected);
			double colorfulness1 = ColorAnalyzer.computeColorfulness(input);
			double colorfulness2 = ColorAnalyzer.computeColorfulness2(input);
			double colorEquilibrium = QuadtreeFeatureComputer.computeEquilibrium(cRoot, centerOfImage);
			double intensityEquilibrium = QuadtreeFeatureComputer.computeEquilibrium(iRoot, centerOfImage);
			double colorHorizontalSymmetry = QuadtreeFeatureComputer.computeHorizontalSymmetry(cRoot);
			double colorVerticalSymmetry = QuadtreeFeatureComputer.computeVerticalSymmetry(cRoot);
			double intensityHorizontalSymmetry = QuadtreeFeatureComputer.computeHorizontalSymmetry(iRoot);
			double intensityVerticalSymmetry = QuadtreeFeatureComputer.computeVerticalSymmetry(iRoot);
			double colorHorizontalBalance = QuadtreeFeatureComputer.computeHorizontalBalance(cRoot);
			double colorVerticalBalance = QuadtreeFeatureComputer.computeVerticalBalance(cRoot);
			double intensityHorizontalBalance = QuadtreeFeatureComputer.computeHorizontalBalance(iRoot);
			double intensityVerticalBalance = QuadtreeFeatureComputer.computeVerticalBalance(iRoot);
			int colorNumQuadTreeLeaves = cRoot.countLeaves() / 4;
			int intensityNumQuadTreeLeaves = iRoot.countLeaves() / 4;
			double colorfulnessScore = -0.676
				+ 2.447 * colorGray
				+ 2.736 * colorWhite
				+ 1.604 * colorMaroon
				+ 1.847 * colorGreen
				- 3.171 * colorLime
				- 3.905 * colorBlue
				+ 1.01 * colorTeal
				+ 0.005 * colorSaturation
				+ 0.03 * colorfulness2
				+ 0.057 * numOfImageArea
				+ 0.000374 * colorNumQuadTreeLeaves
				- 0.000001483 * textArea
				+ 0.000001862 * nonTextLeavesArea;		
			double complexityScore = 0.637
				+ 0.005 * numOfLeaves
				+ 0.052 * numOfTextGroup
				+ 0.056 * numOfImageArea
				+ 0.011 * colorfulness1
				+ 0.005 * colorHue
				+ 0.00001144 * textArea
				+ 0.000004741 * nonTextLeavesArea;
			
			csvWriter.append(
				textArea + ","
				+ nonTextLeavesArea + ","
				+ maxLevel + ","
				+ avgLevel + ","
				+ numOfLeaves + ","
				+ numOf1stLevelNodes + ","
				+ numOf2ndLevelNodes + ","
				+ numOf3rdLevelNodes + ","
				+ percentageLeafArea + ","
				+ percentageLeafArea2 + ","
				+ numOfTextGroup + ","
				+ numOfImageArea + ","
				+ colorfulness1 + ","
				+ colorfulness2 + ","
				+ colorNumQuadTreeLeaves + ","
				+ intensityNumQuadTreeLeaves + ","
				+ colorHorizontalSymmetry + "," 
				+ colorVerticalSymmetry + ","
				+ colorHorizontalBalance + ","
				+ colorVerticalBalance + ","
				+ intensityHorizontalSymmetry + ","
				+ intensityVerticalSymmetry + ","
				+ intensityHorizontalBalance + ","
				+ intensityVerticalBalance + "," 
				+ colorEquilibrium + ","
				+ intensityEquilibrium + ","
				+ colorfulnessScore + ","
				+ complexityScore + "/n"
			);
			csvWriter.flush();
			csvWriter.close();

			// JSON data section
			// GROUP: composite scores
			JSONObject compositeScoresGroup = new JSONObject();
			compositeScoresGroup.put("categoryName", "compositeScores");
			JSONArray compositeScoresItems = new JSONArray();
			compositeScoresItems.add(buildMetricJSON("colorfulnessScore", colorfulnessScore, "valueRange", 0, 10));
			compositeScoresItems.add(buildMetricJSON("complexityScore", complexityScore, "valueRange", 0, 10));
			compositeScoresGroup.put("items", compositeScoresItems);
			metrics.add(compositeScoresGroup);

			// GROUP: text features
			JSONObject textFeaturesGroup = new JSONObject();
			textFeaturesGroup.put("categoryName", "text");
			JSONArray textFeaturesItems = new JSONArray();
			textFeaturesItems.add(buildMetricJSON("textArea", (textArea / (double)(textArea + nonTextLeavesArea)), "percentage"));
			textFeaturesItems.add(buildMetricJSON("nonTextLeavesArea", (nonTextLeavesArea / (double)(textArea + nonTextLeavesArea)), "percentage"));
			textFeaturesItems.add(buildMetricJSON("numOfTextGroup", (double)numOfTextGroup, "value"));
			textFeaturesGroup.put("items", textFeaturesItems);
			metrics.add(textFeaturesGroup);
			
			// GROUP: quadtree decomposition
			JSONObject quadtreeDecompositionGroup = new JSONObject();
			quadtreeDecompositionGroup.put("categoryName", "quadtreeDecomposition");
			JSONArray quadtreeDecompositionItems = new JSONArray();
			quadtreeDecompositionItems.add(buildMetricJSON("maxLevel", (double)maxLevel, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("avgLevel", avgLevel, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("numOfLeaves", (double)numOfLeaves, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("numOf1stLevelNodes", (double)numOf1stLevelNodes, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("numOf2ndLevelNodes", (double)numOf2ndLevelNodes, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("numOf3rdLevelNodes", (double)numOf3rdLevelNodes, "value"));
			quadtreeDecompositionItems.add(buildMetricJSON("percentageLeafArea", percentageLeafArea, "percentage"));
			quadtreeDecompositionItems.add(buildMetricJSON("percentageLeafArea2", percentageLeafArea2, "percentage"));
			quadtreeDecompositionGroup.put("items", quadtreeDecompositionItems);
			metrics.add(quadtreeDecompositionGroup);

			// GROUP: image features
			JSONObject imageGroup = new JSONObject();
			imageGroup.put("categoryName", "images");
			JSONArray imageItems = new JSONArray();
			imageItems.add(buildMetricJSON("numOfImageArea", (double)numOfImageArea, "value"));
			imageGroup.put("items", imageItems);
			metrics.add(imageGroup);

			// GROUP: other
			JSONObject otherGroup = new JSONObject();
			otherGroup.put("categoryName", "other");
			JSONArray otherItems = new JSONArray();
			otherItems.add(buildMetricJSON("colorfulness1", colorfulness1, "value"));
			otherItems.add(buildMetricJSON("colorfulness2", colorfulness2, "value"));
			otherItems.add(buildMetricJSON("colorNumQuadTreeLeaves", colorNumQuadTreeLeaves, "value"));
			otherItems.add(buildMetricJSON("intensityNumQuadTreeLeaves", intensityNumQuadTreeLeaves, "value"));
			otherGroup.put("items", otherItems);
			metrics.add(otherGroup);

			// GROUP: equilibrium
			JSONObject equilibriumGroup = new JSONObject();
			equilibriumGroup.put("categoryName", "equilibrium");
			JSONArray equilibriumItems = new JSONArray();
			equilibriumItems.add(buildMetricJSON("colorEquilibrium", colorEquilibrium, "valueRange", 0, 1));
			equilibriumItems.add(buildMetricJSON("intensityEquilibrium", intensityEquilibrium, "valueRange", 0, 1));
			equilibriumGroup.put("items", equilibriumItems);
			metrics.add(equilibriumGroup);

			// GROUP: symmetry
			JSONObject symmetryGroup = new JSONObject();
			symmetryGroup.put("categoryName", "symmetry");
			JSONArray symmetryItems = new JSONArray();
			symmetryItems.add(buildMetricJSON("colorHorizontalSymmetry", colorHorizontalSymmetry, "valueRange", 0, 1));
			symmetryItems.add(buildMetricJSON("colorVerticalSymmetry", colorVerticalSymmetry, "valueRange", 0, 1));
			symmetryItems.add(buildMetricJSON("intensityHorizontalSymmetry", intensityHorizontalSymmetry, "valueRange", 0, 1));
			symmetryItems.add(buildMetricJSON("intensityVerticalSymmetry", intensityVerticalSymmetry, "valueRange", 0, 1));
			symmetryGroup.put("items", symmetryItems);
			metrics.add(symmetryGroup);

			// GROUP: balance
			JSONObject balanceGroup = new JSONObject();
			balanceGroup.put("categoryName", "balance");
			JSONArray balanceItems = new JSONArray();
			balanceItems.add(buildMetricJSON("colorHorizontalBalance", colorHorizontalBalance, "valueRange", 0, 1));
			balanceItems.add(buildMetricJSON("colorVerticalBalance", colorVerticalBalance, "valueRange", 0, 1));
			balanceItems.add(buildMetricJSON("intensityHorizontalBalance", intensityHorizontalBalance, "valueRange", 0, 1));
			balanceItems.add(buildMetricJSON("intensityVerticalBalance", intensityVerticalBalance, "valueRange", 0, 1));
			balanceGroup.put("items", balanceItems);
			metrics.add(balanceGroup);
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		
		JSONObject returnJSON = new JSONObject();
		returnJSON.put("metrics", metrics);
		returnJSON.put("isCropped", isCropped);

		return returnJSON;
	}

	public static void decomposeSingleImage(BufferedImage inputImage,
			String inputFileName, String outputFileName) throws IOException {

		try {
			XYDecompositionStrategy strategy = new DefaultXYDecompositionStrategy() {

				@Override
				public int getMaxLevel() {
					return 10;
				}

				@Override
				public boolean isSplittingHorizontally() {
					return true;
				}

				@Override
				public boolean isSplittingVertically() {
					return true;
				}

				@Override
				public int getMinSeperatorSize() {
					return 4;
				}
			};

			XYDecomposer d = new XYDecomposer();
			org.sikuli.design.structure.Block root = d.decompose(inputImage, strategy);

			root.filterOutSmallBlocks();

			XYTextDetector td = new XYTextDetector(root, inputImage);
			Block rootWithTextdetected = td.detect();
	
			rootWithTextdetected.toXML(inputFileName, outputFileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}