package com.upload;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

public abstract class FileUploadBase {
	
    protected Map<String, String> parameters = new HashMap<String, String>();// save general form
    
    protected String encoding = "UTF-8"; 

    protected UploadFileFilter filter = null; 
    
    protected int sizeThreshold = DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD;

    protected long sizeMax = -1;//no limit for the size

    protected File repository;
    
    public String getParameter(String key){
        return parameters.get(key);
    }

    public String getEncoding(){
        return encoding;
    }

    public void setEncoding(String encoding){
        this.encoding = encoding;
    }

    /** 
     * getting the maximum size of the upload file
     * @return
     */
    public long getSizeMax(){
        return sizeMax;
    }

    /**
     * setting the maximum size of the upload file
     * @param sizeMax
     */
    public void setSizeMax(long sizeMax){
        this.sizeMax = sizeMax;
    }

    /** 
     * getting size of the temporary file
     */
    public int getSizeThreshold(){
        return sizeThreshold;
    }
    
    /** 
     * setting size of the temporary file
     */
    public void setSizeThreshold(int sizeThreshold){
        this.sizeThreshold = sizeThreshold;
    }

    /** 
     * getting the repository of the temporary file
     */
    public File getRepository() {
        return repository;
    }

    /** 
     * setting the repository of the temporary file
     */
    public void setRepository(File repository) {
        this.repository = repository;
    }
    
    /** 
     * getting parameters of the form
     * @return
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * getting the file filter
     * @return
     */
    public UploadFileFilter getFilter() {
        return filter;
    }

    /** 
     * setting the file filters
     * @param filter
     */
    public void setFilter(UploadFileFilter filter) {
        this.filter = filter;
    }
    
    /** 
     * checking if it is a valid file
     * @param item
     * @return
     */
    protected boolean isValidFile(FileItem item){
        return !(item == null || item.getName() == "" || item.getSize() == 0 || (filter != null && !filter.accept(item.getName())));
    }
}

