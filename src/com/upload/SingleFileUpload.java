package com.upload;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FilenameUtils;

/**
 * support single file uploaded
 * @author Administrator
 */

public class SingleFileUpload extends FileUploadBase{
    
	private static SingleFileUpload sf = null;
	private FileItem fileItem;
    
    private SingleFileUpload(){
    	super();
    }
    
    public static SingleFileUpload getInstance(){
    	sf = new SingleFileUpload();
    	return sf;
    }
    /**
     * @param request
     * @throws UnsupportedEncodingException
     */
    public void parseRequest(HttpServletRequest request)
            throws UnsupportedEncodingException{

        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(sizeThreshold);
        if (repository != null)
            factory.setRepository(repository);

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding(encoding);
        
        try{
            List items = upload.parseRequest(request);

            for(int i=0;i<items.size();i++){
            	FileItem item = (FileItem)items.get(i);
                if (item.isFormField()){
                    String fieldName = item.getFieldName();
                    String value = item.getString(encoding);
                    parameters.put(fieldName, value);
                } else {
                    if (!super.isValidFile(item)){
                        continue;
                    }
                    if (fileItem == null)
                        fileItem = item;
                }
            }

        } catch (FileUploadException e) {
            e.printStackTrace();
        }
    }
    /**
     * path of saving the uploaded image
     * @param request
     * @throws UnsupportedEncodingException
     */
    public void saveUploadFile(HttpServletRequest request,String savePath)
            throws UnsupportedEncodingException{

        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(sizeThreshold);
        if (repository != null)
            factory.setRepository(repository);

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding(encoding);
        
        try{
            List items = upload.parseRequest(request);

            for(int i=0;i<items.size();i++){
            	FileItem item = (FileItem)items.get(i);
                if (item.isFormField()){
                    String fieldName = item.getFieldName();
                    String value = item.getString(encoding);
                    parameters.put(fieldName, value);
                } else {
                    if (!super.isValidFile(item)){
                        continue;
                    }
                    if (fileItem == null)
                        fileItem = item;
                    try {
                    	String fileName = fileItem.getName();
                    	//get the name of the uploaded image
                    	if (fileName != null) {
							fileName = FilenameUtils.getName(fileName);
						}
						BufferedInputStream bis = new BufferedInputStream(fileItem.getInputStream());
						BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(savePath+fileName)));
						Streams.copy(bis, bos, true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
        }
    }
    /** 
     * uploading images, call parseRequest(HttpServletRequest request) method before using it
     * @param fileName absolute file path
     * @throws Exception
     * @return
     */
    public File upload(String fileName) throws Exception{
        File file = new File(fileName);
        uploadFile(file);
        return file;
    }

    /**
     *  uploading images, call parseRequest(HttpServletRequest request) method before using it(HttpServletRequest request)
     * @param parent path of the file repository
     * @throws Exception
     */
    public File upload(File parent) throws Exception{
        if (fileItem == null)
            return null;

        String fileName = fileItem.getName();
    	//get the name of the uploaded file
    	if (fileName != null) {
			fileName = FilenameUtils.getName(fileName);
		}
        File file = new File(parent, fileName);
        uploadFile(file);
        return file;
    }
    
    private void uploadFile(File file) throws Exception {
        if (fileItem == null)
            return;

        long fileSize = fileItem.getSize();
        if (sizeMax > -1 && fileSize > super.sizeMax){
            String message = "size of uploaded image is larger than  "+sizeMax;
                    
            throw new org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException(message, fileSize, super.sizeMax);
        }
        //String name = fileItem.getName();
        try {
        	fileItem.write(file);
		} catch (Exception e) {
			// TODO: handle exception
			file.getParentFile().mkdirs();
			fileItem.write(file);
		}
    }
    
    /**
     * getting file item
     * call parseRequest(HttpServletRequest request) method first
     * @return
     */
    public FileItem getFileItem(){
        return fileItem;
    }
    /**
     * getting the byte of file
     * call parseRequest(HttpServletRequest request) method first
     * @return
     */
    public byte[] getFileItemBytes(){
    	try {
			InputStream is = getFileItemStream();
			if (is != null) {
				int length = is.available();
    			byte[] bytes = new byte[length];
    			is.read(bytes);
    			is.close();
    			return bytes;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    /**
     * get input stream of the file
     * get parseRequest(HttpServletRequest request) method first
     * @return
     */
    public InputStream getFileItemStream(){
    	try {
    		if (fileItem!=null) {
    			return fileItem.getInputStream();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    /**
     * clear temporary data
     */
    public void clearData(){
    	parameters.clear();
    	parameters = null;
    	fileItem = null;
    }   
}

