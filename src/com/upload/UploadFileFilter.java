package com.upload;

public interface UploadFileFilter{
    /**
     * Accepted or not filter
     * @param filename file name
     * @return
     */
    public boolean accept(String filename);
}

